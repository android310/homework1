package com.example.danco.homework2.provider;

import android.content.ContentValues;

import java.util.Date;

public class DataUtilities {

    static ContentValues createInsertValues() {
        ContentValues values = new ContentValues();
        values.put(WeatherContract.CurrentConditionsColumns.ADDED, new Date().getTime());
        values.put(WeatherContract.CurrentConditionsColumns.HUMIDITY, 50);
        values.put(WeatherContract.CurrentConditionsColumns.WIND, 10.0);
        values.put(WeatherContract.CurrentConditionsColumns.ICON, "icon_01d");
        values.put(WeatherContract.CurrentConditionsColumns.TEMP, 57.26);
        values.put(WeatherContract.CurrentConditionsColumns.TEMP_MIN, 57.26);
        values.put(WeatherContract.CurrentConditionsColumns.TEMP_MAX, 58.00);
        values.put(WeatherContract.CurrentConditionsColumns.DESCRIPTION, "scattered clouds");
        values.put(WeatherContract.CurrentConditionsColumns.CITY, "Seattle");
        values.put(WeatherContract.CurrentConditionsColumns.TIMESTAMP, 1429294076);
        return values;
    }

    static ContentValues[] createBulkInsertValues() {
        ContentValues[] valuesArray = new ContentValues[2];
        valuesArray[0] = createInsertValues();
        ContentValues values = new ContentValues();
        values.put(WeatherContract.CurrentConditionsColumns.ADDED, new Date().getTime());
        values.put(WeatherContract.CurrentConditionsColumns.HUMIDITY, 50);
        values.put(WeatherContract.CurrentConditionsColumns.WIND, 10.0);
        values.put(WeatherContract.CurrentConditionsColumns.ICON, "icon_01d");
        values.put(WeatherContract.CurrentConditionsColumns.TEMP, 57.26);
        values.put(WeatherContract.CurrentConditionsColumns.TEMP_MIN, 57.26);
        values.put(WeatherContract.CurrentConditionsColumns.TEMP_MAX, 58.00);
        values.put(WeatherContract.CurrentConditionsColumns.DESCRIPTION, "scattered clouds");
        values.put(WeatherContract.CurrentConditionsColumns.CITY, "Seattle");
        values.put(WeatherContract.CurrentConditionsColumns.TIMESTAMP, 1429294076);
        valuesArray[1] = values;
        return valuesArray;
    }

    static ContentValues createUpdateValues() {
        ContentValues values = new ContentValues();
        values.put(WeatherContract.CurrentConditionsColumns.DESCRIPTION, "partly sunny");
        return values;
    }

    public static ContentValues[] mergeValues(ContentValues[] targetArray, ContentValues source) {
        for (ContentValues target : targetArray) {
            mergeValues(target, source);
        }
        return targetArray;
    }

    public static ContentValues mergeValues(ContentValues target, ContentValues source) {
        target.putAll(source);
        return target;
    }
}
