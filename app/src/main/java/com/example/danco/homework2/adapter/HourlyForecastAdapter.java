package com.example.danco.homework2.adapter;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.danco.homework2.R;
import com.example.danco.homework2.provider.WeatherContract;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author danco on 4/18/15.
 */
public class HourlyForecastAdapter extends CursorAdapter {
    private static final String TAG = HourlyForecastAdapter.class.getSimpleName() + ".TAG";

    public static final String[] PROJECTION = {
            WeatherContract.HourlyForecastColumns._ID,
            WeatherContract.HourlyForecastColumns.ICON,
            WeatherContract.HourlyForecastColumns.DESCRIPTION,
            WeatherContract.HourlyForecastColumns.TEMP_MIN,
            WeatherContract.HourlyForecastColumns.TEMP_MAX,
            WeatherContract.HourlyForecastColumns.DT_TXT
    };

    private static final int ICON_POS = 1;
    private static final int DESCRIPTION_POS = 2;
    private static final int TEMP_MIN_POS = 3;
    private static final int TEMP_MAX_POS = 4;
    private static final int DT_TXT_POS = 5;


    public HourlyForecastAdapter(Context context) {
        super(context, null, 0);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.hourly_forecast,
                parent, false);
        view.setTag(new ViewHolder(view));

        return view;
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.description.setText(cursor.getString(DESCRIPTION_POS));
        holder.min.setText(cursor.getString(TEMP_MIN_POS));
        holder.max.setText(cursor.getString(TEMP_MAX_POS));
        holder.minLabel.setText("Min temp: ");
        holder.maxLabel.setText("Max temp: ");

        DateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        Date forecastDate = null;
        try {
            forecastDate = dateParser.parse(cursor.getString(DT_TXT_POS));
        } catch (ParseException e) {
            Log.e(TAG, "Unable to parse date from " + cursor.getString(DT_TXT_POS));
        }
        DateFormat dayFormatter = new SimpleDateFormat("EEE K a", Locale.US);
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        calendar.setTime(forecastDate);
        String timeStr = dayFormatter.format(forecastDate);
        timeStr = timeStr.replace("0 AM", "Midnight");
        timeStr = timeStr.replace("0 PM", "Noon");
        holder.dayAndTime.setText(timeStr);
    }


    /* package */ class ViewHolder {
        final ImageView icon;
        final TextView dayAndTime;
        final TextView description;
        final TextView minLabel;
        final TextView maxLabel;
        final TextView min;
        final TextView max;
        ViewHolder(View view) {
            icon = (ImageView) view.findViewById(R.id.weatherIcon);
            dayAndTime = (TextView) view.findViewById(R.id.dayAndTime);
            description = (TextView) view.findViewById(R.id.forecastDescription);
            minLabel = (TextView) view.findViewById(R.id.minTempLabel);
            maxLabel = (TextView) view.findViewById(R.id.maxTempLabel);
            min = (TextView) view.findViewById(R.id.minTemp);
            max = (TextView) view.findViewById(R.id.maxTemp);
        }
    }
}
