package com.example.danco.homework2.service;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.danco.homework2.R;
import com.example.danco.homework2.jsonprocessor.BaseProcessor;
import com.example.danco.homework2.jsonprocessor.CurrentConditionsProcessor;
import com.example.danco.homework2.jsonprocessor.DailyForecastProcessor;
import com.example.danco.homework2.jsonprocessor.HourlyForecastProcessor;
import com.example.danco.homework2.network.CacheUtils;
import com.example.danco.homework2.network.NetworkUtils;
import com.example.danco.homework2.provider.WeatherContract;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.security.ProviderInstaller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;


/**
 * IntentService to fetch OpenWeatherMap data from the network and store in the database.
 * Also removes old Weather data that is no longer needed.
 */
public class NetworkIntentService extends IntentService {
    private static final String TAG = "NetworkIntentService";

    private static final String ACTION_LOAD = NetworkIntentService.class.getName() + ".action_load";
    private static final String EXTRA_BACKOFF = NetworkIntentService.class.getName() + ".backoff";

    private static final String WHERE_WEATHER_STALE =
            WeatherContract.CurrentConditionsColumns.ADDED + " < ?";
    private static long DEFAULT_BACKOFF = 500;

    private long backoff = DEFAULT_BACKOFF;

    public static void startService(Context context) {
        startService(context, DEFAULT_BACKOFF);
    }

    private static void startService(Context context, long backoff) {
        Intent intent = new Intent(context, NetworkIntentService.class);
        intent.setAction(ACTION_LOAD);
        intent.putExtra(EXTRA_BACKOFF, backoff);
        context.startService(intent);
    }

    public NetworkIntentService() {
        super("NetworkIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (ensureLatestSSL()) {
            Log.d(TAG, "starting");
            if (intent != null) {
                final String action = intent.getAction();
                backoff = intent.getLongExtra(EXTRA_BACKOFF, DEFAULT_BACKOFF);
                if (ACTION_LOAD.equals(action)) {
                    handleActionLoad(WeatherContract.CURRENT_CONDITIONS_URI);
                    handleActionLoad(WeatherContract.DAILY_FORECAST_URI);
                    handleActionLoad(WeatherContract.HOURLY_FORECAST_URI);
                }
            }
            Log.d(TAG, "done");
        }
    }


    private boolean ensureLatestSSL() {

        try {
            // Ensure the latest SSL per
            // http://developer.android.com/training/articles/security-gms-provider.html
            ProviderInstaller.installIfNeeded(this);
            return true;
        }
        catch (GooglePlayServicesRepairableException e) {
            // Since this is a background service, show a notification
            GooglePlayServicesUtil.showErrorNotification(e.getConnectionStatusCode(), this);
            Log.d(TAG, "Repairable error updating SSL");
            return false;
        }
        catch (GooglePlayServicesNotAvailableException e) {
            // Since this is a background service, show a notification
            GooglePlayServicesUtil.showErrorNotification(e.errorCode, this);
            Log.d(TAG, "Missing play servers updating SSL");
            return false;
        }
    }


    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionLoad(final Uri uri) {
        CacheUtils.initializeCache(this);

        // No network, we should get out
        if (NetworkUtils.isNotConnected(this)) {
            delayAndRetry();
            return;
        }

        //no need to make the API call if we have fresh data
        if (isDataCurrent(uri)) {
            return;
        }

        ContentValues[] values = null;
        try {
            String type = getContentResolver().getType(uri);
            URL url = null;

            switch (type) {
                case WeatherContract.CURRENT_CONDITIONS_TYPE:
                    url = buildCurrentConditionsUrl();
                    values = performGet(url, CurrentConditionsProcessor.newInstance());
                    break;
                case WeatherContract.DAILY_FORECAST_TYPE:
                    url = buildDailyForecastUrl();
                    values = performGet(url, DailyForecastProcessor.newInstance());
                    break;
                case WeatherContract.HOURLY_FORECAST_TYPE:
                    url = buildHourlyForecastUrl();
                    values = performGet(url, HourlyForecastProcessor.newInstance());
                    break;
                default:
                    throw new UnsupportedOperationException("Unknown uri: " + uri);

            }
            if (values != null && values.length > 0) {
                purgeOldData(uri);
                getContentResolver().bulkInsert(uri, values);
            }
        }
        catch (Exception e) {
            Log.w(TAG, "Unexpected error", e);
        }

        CacheUtils.logCache();
    }


    private void purgeOldData(Uri uri) {
        Calendar expired;
        String[] selectionArgs;
        int rows;
        //we have new rows to add, delete the old stuff
        expired = Calendar.getInstance();
        selectionArgs = new String[]{Long.toString(expired.getTimeInMillis())};
        rows = getContentResolver().delete(uri, WHERE_WEATHER_STALE, selectionArgs);
        Log.d(TAG, "Purged rows: " + rows);
    }


    private boolean isDataCurrent(Uri uri) {

        boolean isDataCurrent =false;

        Cursor cursor = getContentResolver().query(uri,
                new String[]{WeatherContract.WeatherColumns.ADDED}, null, null, null);
        if (cursor.moveToFirst()) {
            int addedTimeIndex = cursor.getColumnIndex(WeatherContract.WeatherColumns.ADDED);
            long added = cursor.getLong(addedTimeIndex);
            Date addedDate = new Date(added);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, -10);
            // this doesn't work, but I don't know why not:
            // isDataCurrent = calendar.before(addedDate);
            isDataCurrent = calendar.getTimeInMillis() < addedDate.getTime();
        }

        return isDataCurrent;
    }


    private URL buildCurrentConditionsUrl() throws MalformedURLException {
        // Using URI builder to ensure url and parameter are properly encoded
        // could buildUpon a base Uri for part of this...
        Uri uri = new Uri.Builder()
                .scheme("http")
                .authority("api.openweathermap.org")
                .appendPath("data").appendPath("2.5")
                .appendPath("weather")
                .appendQueryParameter("id", "5809844")
                .appendQueryParameter("units", "imperial")
                .appendQueryParameter("APPID", getResources().getString(R.string.owm_key))
                .build();


        Log.d(TAG, uri.toString());
        return new URL(uri.toString());
    }

    private URL buildDailyForecastUrl() throws MalformedURLException {
        // Using URI builder to ensure url and parameter are properly encoded
        Uri uri = new Uri.Builder()
                .scheme("http")
                .authority("api.openweathermap.org")
                .appendPath("data").appendPath("2.5")
                .appendPath("forecast")
                .appendPath("daily")
                .appendQueryParameter("id", "5809844")
                .appendQueryParameter("units", "imperial")
                .appendQueryParameter("APPID", getResources().getString(R.string.owm_key))
                .build();


        Log.d(TAG, uri.toString());
        return new URL(uri.toString());
    }

    private URL buildHourlyForecastUrl() throws MalformedURLException {
        Uri uri = new Uri.Builder()
                .scheme("http")
                .authority("api.openweathermap.org")
                .appendPath("data").appendPath("2.5")
                .appendPath("forecast")
                .appendQueryParameter("id", "5809844")
                .appendQueryParameter("units", "imperial")
                .appendQueryParameter("APPID", getResources().getString(R.string.owm_key))
                .build();


        Log.d(TAG, uri.toString());
        return new URL(uri.toString());
    }

    private ContentValues[] performGet(URL url, BaseProcessor processor) throws IOException {

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            urlConnection.setRequestMethod("GET");

            // Force network request, ignoring cache
            // urlConnection.addRequestProperty("Cache-Control", "no-cache");

            // Check to see if cache is stale. Forces a server validation of the cache
            // and reuse cache if hasn't changed.
            // urlConnection.addRequestProperty("Cache-Control", "max-age=0");

            // Reuse data only if its cached and valid. Can be useful for when offline. However,
            // if the cache data has expired, the cache will respond with status code 504 per
            // the RFC specification. (Note: for this assignment is a way to see the backoff logic)
            // urlConnection.addRequestProperty("Cache-Control", "only-if-cached");

            // Allow stale cache data. Useful if offline to allow re-using data from the cache
            // even if it is stale. Better version of only-if-cached as it will return the stale
            // data up to the time limit.
            // urlConnection.addRequestProperty("Cache-Control", "max-stale=600000");

            // Using 15 sec timeouts.
            urlConnection.setConnectTimeout(15000);
            urlConnection.setReadTimeout(15000);
            urlConnection.connect();

            int status = urlConnection.getResponseCode();

            // For the Seattle 911 api, this response, means retry...
            if (status == HttpURLConnection.HTTP_ACCEPTED) {
                delayAndRetry();
            }
            // For Ok, process result
            if (status == HttpURLConnection.HTTP_OK) {
                return processor.buildContentValues(urlConnection.getInputStream());
            }

            Log.w(TAG, "status: " + status);

            // Only logging contents. Depending on the API, there may be other info that is
            // useful to the application, developer, or possibly the user. For example if
            // you change the url and misspell "event" in the column name it indicates that
            // the column was not found.
            logErrorStream(urlConnection.getErrorStream());
            delayAndRetry();
            return null;
        }
        finally {
            // Return connection back to the pool or to be closed.
            urlConnection.disconnect();
        }
    }


    private void delayAndRetry() {

        double RANDOM_FACTOR = 0.5d; // 50% below + 50% above
        double BACKOFF_INTERVAL = 1.5d; // 50% backoff rate
        double MAX_BACKOFF = 15000; // Max backoff

        double minInterval = backoff - backoff * RANDOM_FACTOR;
        double maxInterval = backoff + backoff * RANDOM_FACTOR;
        double random = Math.random();
        long delay = Math.round(minInterval + random * (maxInterval - minInterval + 1));

        try {
            Log.d(TAG, "Sleeping: " + delay);
            Thread.sleep(delay);
        }
        catch (InterruptedException e) {
            // do nothing;
        }

        // Compute next backoff, use random so not all apps hit server at same time
        long newBackoff = Math.round(backoff * BACKOFF_INTERVAL);
        if (newBackoff < MAX_BACKOFF) {
            Log.d(TAG, "New backoff: " + newBackoff);
            startService(this, newBackoff);
        }
        else {
            Log.d(TAG, "Hit max backoff, stopping");
        }
    }


    private void logErrorStream(@Nullable InputStream errorStream) throws IOException {
        if (errorStream == null) {
            return;
        }

        StringBuilder builder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(errorStream, "UTF-8"));
        try {
            String result;
            while ((result = reader.readLine()) != null) {
                builder.append(result);
            }
        }
        finally {
            reader.close();
        }

        Log.w(TAG, builder.toString());
    }
}
