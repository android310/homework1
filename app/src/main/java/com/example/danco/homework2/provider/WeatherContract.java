package com.example.danco.homework2.provider;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

import com.example.danco.homework2.BuildConfig;

/**
 * @author danco on 4/14/15.
 */
public class WeatherContract {

    public interface WeatherColumns extends BaseColumns {
        String ADDED = "time_added";
        String CITY_ID = "city_id";
        String CITY = "city";
        String TIMESTAMP = "dt";
    }

    public interface CurrentConditionsColumns extends WeatherColumns {
        String TEMP = "temp";
        String TEMP_MIN = "temp_min";
        String TEMP_MAX = "temp_max";
        String DESCRIPTION = "description";
        String HUMIDITY = "humidity";
        String WIND = "wind";
        String ICON = "icon";
//        String JSON = "json";
    }

    public interface DailyForecastColumns extends CurrentConditionsColumns {
        String MIN = "min";
        String MAX = "max";
    }

    public interface HourlyForecastColumns extends CurrentConditionsColumns {
        String DT_TXT = "dt_txt";
        String DAY_OF_WEEK = "day";
    }

    public interface WeatherJson {
        String CITY = "city";
        String NAME = "name";
        String WEATHER = "weather";
        String TEMP = "temp";
        String TIMESTAMP = "dt";
        String WEATHER_ICON = "icon";
        String MAIN = "main";
        String WEATHER_DESCRIPTION = "description";
        String DAY_LIST = "list";
        String WIND = "wind";
        String SPEED = "speed";
        String HUMIDITY = "humidity";
    }

    public interface CurrentConditionsJson extends WeatherJson {
        String TEMP_MIN = "temp_min";
        String TEMP_MAX = "temp_max";
    }

    public interface DailyForecastJson extends WeatherJson {
        String TEMP_DAY = "day";
        String TEMP_MIN = "min";
        String TEMP_MAX = "max";
        String TEMP_NIGHT = "night";
        String TEMP_EVE = "eve";
        String TEMP_MORN = "morn";
    }

    public interface HourlyForecastJson extends CurrentConditionsJson {
        String HOURLY_TIMESTAMP = "dt_txt";
        String DAY = "day";
    }

    public static final long NO_ID = -1;


    /* package */ static final String CURRENT_CONDITIONS_TABLE = "current_conditions";
    /* package */ static final String DAILY_FORECAST_TABLE = "daily_forecast";
    /* package */ static final String HOURLY_FORECAST_TABLE = "hourly_forecast";

    /* package */ static final String AUTHORITY =
            BuildConfig.APPLICATION_ID + ".provider.WeatherProvider";


    // Should have one URI per table (or path), supported by this contract. In most cases the
    // contract should only support one table in which case the BASE_URI can be the URI.
    //
    // However, you may have situations where you need more than one.
    // In that case build upon the base URI and add a "path" for each table you support.
    // Will have final form of:
    // content://com.example.danco.homework.provider.MyProvider/<table_name>
    //
    // public static final Uri URI = BASE_URI.buildUpon()
    //        .appendPath(TABLE)
    //        .build();

    // In this provider's case there is only one table, so lets just use the base uri. It has
    // the advantage is the "table" name can not be guessed if exported.

    /* package */ static final Uri BASE_URI = new Uri.Builder()
            .scheme(ContentResolver.SCHEME_CONTENT)
            .authority(AUTHORITY)
            .build();

    /**
     * Uris for each supported table
     */
    public static final Uri CURRENT_CONDITIONS_URI = BASE_URI.buildUpon()
            .appendPath(CURRENT_CONDITIONS_TABLE)
            .build();

    public static final Uri DAILY_FORECAST_URI = BASE_URI.buildUpon()
            .appendPath(DAILY_FORECAST_TABLE)
            .build();

    public static final Uri HOURLY_FORECAST_URI = BASE_URI.buildUpon()
            .appendPath(HOURLY_FORECAST_TABLE)
            .build();


    /**
     * Types for the collection and specific row
     */
    public static final String CURRENT_CONDITIONS_TYPE =
            ContentResolver.CURSOR_DIR_BASE_TYPE +
            "/" + AUTHORITY + "/" + CURRENT_CONDITIONS_TABLE;

    public static final String CURRENT_CONDITIONS_ITEM_TYPE =
            ContentResolver.CURSOR_ITEM_BASE_TYPE +
            "/" + AUTHORITY + "/" + CURRENT_CONDITIONS_TABLE;


    public static final String DAILY_FORECAST_TYPE =
            ContentResolver.CURSOR_DIR_BASE_TYPE +
            "/" + AUTHORITY + "/" + DAILY_FORECAST_TABLE;

    public static final String DAILY_FORECAST_ITEM_TYPE =
            ContentResolver.CURSOR_ITEM_BASE_TYPE +
            "/" + AUTHORITY + "/" + DAILY_FORECAST_TABLE;


    public static final String HOURLY_FORECAST_TYPE =
            ContentResolver.CURSOR_DIR_BASE_TYPE +
            "/" + AUTHORITY + "/" + HOURLY_FORECAST_TABLE;

    public static final String HOURLY_FORECAST_ITEM_TYPE =
            ContentResolver.CURSOR_ITEM_BASE_TYPE +
            "/" + AUTHORITY + "/" + HOURLY_FORECAST_TABLE;


    /**
     * Table definitions
     */
    /* package */ static final String CREATE_CURRENT_CONDITIONS_TABLE =
            "CREATE TABLE " + CURRENT_CONDITIONS_TABLE + " ( " +
                    BaseColumns._ID + " INTEGER PRIMARY KEY, " +
                    CurrentConditionsColumns.ADDED + " NUMERIC NOT NULL, " +
                    CurrentConditionsColumns.CITY + " TEXT NOT NULL, " +
                    CurrentConditionsColumns.TIMESTAMP + " NUMERIC NOT NULL, " +
                    CurrentConditionsColumns.TEMP + " REAL NOT NULL, " +
                    CurrentConditionsColumns.TEMP_MIN + " REAL NOT NULL, " +
                    CurrentConditionsColumns.TEMP_MAX + " REAL NOT NULL, " +
                    CurrentConditionsColumns.DESCRIPTION + " STRING NOT NULL, " +
                    CurrentConditionsColumns.HUMIDITY + " NUMERIC NOT NULL, " +
                    CurrentConditionsColumns.WIND + " REAL NOT NULL, " +
                    CurrentConditionsColumns.ICON + " STRING NOT NULL, " +
                    "FOREIGN KEY(" + CurrentConditionsColumns.CITY_ID + ") REFERENCES " +
                    CityContract.TABLE + "(" + BaseColumns._ID + ") ON DELETE CASCADE " +
                    " )";


    /* package */ static final String CREATE_DAILY_FORECAST_TABLE =
            "CREATE TABLE " + DAILY_FORECAST_TABLE + " ( " +
                    BaseColumns._ID + " INTEGER PRIMARY KEY, " +
                    DailyForecastColumns.ADDED + " NUMERIC NOT NULL, " +
                    DailyForecastColumns.CITY + " TEXT NOT NULL, " +
                    DailyForecastColumns.TIMESTAMP + " NUMERIC NOT NULL, " +
                    DailyForecastColumns.MIN + " REAL NOT NULL, " +
                    DailyForecastColumns.MAX + " REAL NOT NULL, " +
                    DailyForecastColumns.DESCRIPTION + " STRING NOT NULL, " +
                    DailyForecastColumns.HUMIDITY + " REAL NOT NULL, " +
                    DailyForecastColumns.WIND + " REAL NOT NULL, " +
                    DailyForecastColumns.ICON + " STRING NOT NULL" +
                    " )";


    /* package */ static final String CREATE_HOURLY_FORECAST_TABLE =
            "CREATE TABLE " + HOURLY_FORECAST_TABLE + " ( " +
                    BaseColumns._ID + " INTEGER PRIMARY KEY, " +
                    HourlyForecastColumns.ADDED + " NUMERIC NOT NULL, " +
                    HourlyForecastColumns.CITY + " TEXT NOT NULL, " +
                    HourlyForecastColumns.TIMESTAMP + " NUMERIC NOT NULL, " +
                    HourlyForecastColumns.TEMP + " REAL NOT NULL, " +
                    HourlyForecastColumns.TEMP_MIN + " REAL NOT NULL, " +
                    HourlyForecastColumns.TEMP_MAX + " REAL NOT NULL, " +
                    HourlyForecastColumns.DESCRIPTION + " STRING NOT NULL, " +
                    HourlyForecastColumns.DT_TXT + " STRING NOT NULL, " +
                    HourlyForecastColumns.DAY_OF_WEEK + " STRING NOT NULL, " +
                    HourlyForecastColumns.ICON + " STRING NOT NULL" +
                    " )";

    // Adding an index for the FK. This dramatically helps performance and prevent global
    //  table locking.
    /* package */ static final String CITY_ID_FK_INDEX = "CITY_ID_FK_IDX";

    /* package */ static final String CREATE_CITY_ID_FK_INDEX =
            "CREATE INDEX IF NOT EXISTS " + CITY_ID_FK_INDEX + " ON " + CURRENT_CONDITIONS_TABLE +
                    " (" + CurrentConditionsColumns.CITY_ID + ")";

}
