package com.example.danco.homework2.jsonprocessor;

import android.content.ContentValues;
import android.util.JsonReader;
import android.util.JsonToken;

import com.example.danco.homework2.provider.WeatherContract;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * @author danco on 4/18/15.
 */
public abstract class BaseProcessor {
    public ContentValues[] buildContentValues(InputStream stream) throws IOException {

        // Ideally determine encoding from the HTTP response headers vs hardcoding to UTF-8.
        // However, UTF-8 is also most common format.
        JsonReader jsonReader = new JsonReader(new InputStreamReader(stream, "UTF-8"));

        // Note: AS will indicate that you can use try with resources because Java 7 is
        // target. However, try with resources is not compatible prior to SDK 19.
        try {
            return processArray(jsonReader);
        }
        finally {
            jsonReader.close();
        }
    }

    protected ContentValues[] processArray(JsonReader jsonReader) throws IOException {
        ArrayList<ContentValues> list = new ArrayList<>();

        if (jsonReader.peek() == JsonToken.BEGIN_ARRAY) {
            jsonReader.beginArray();
            while (jsonReader.hasNext()) {
                list.add(processObject(jsonReader));
            }
            jsonReader.endArray();
        } else {
            list.add(processObject(jsonReader));
        }

        ContentValues[] values = new ContentValues[list.size()];
        return list.toArray(values);
    }

    protected abstract ContentValues processObject(JsonReader reader) throws IOException;


    /**
     * Process the weather json object in the current conditions json
     * @param jsonReader a reader for the json object
     * @return all pertinent content values from the weather object
     */
    protected static ContentValues processWeather(JsonReader jsonReader) throws IOException {
        ContentValues values = new ContentValues();

        jsonReader.beginArray();
        jsonReader.beginObject();

        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();
            switch (name) {
                case WeatherContract.CurrentConditionsJson.WEATHER_DESCRIPTION:
                    values.put(WeatherContract.CurrentConditionsColumns.DESCRIPTION,
                            jsonReader.nextString());
                    break;
                case WeatherContract.CurrentConditionsJson.WEATHER_ICON:
                    values.put(WeatherContract.CurrentConditionsColumns.ICON,
                            jsonReader.nextString());
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }

        jsonReader.endObject();
        jsonReader.endArray();

        return values;
    }


    protected String processCity(JsonReader reader) throws IOException {
        String cityName = null;

        reader.beginObject();

        while (reader.hasNext()) {
            String name = reader.nextName();
            switch (name) {
                case WeatherContract.WeatherJson.NAME:
                    cityName = reader.nextString();
                    // could break out here but seems like the pattern is more important than the
                    // performance gain by doing that in this case...
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }

        reader.endObject();

        return cityName;
    }
}
