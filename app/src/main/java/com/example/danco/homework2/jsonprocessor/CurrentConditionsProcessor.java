package com.example.danco.homework2.jsonprocessor;

import android.content.ContentValues;
import android.util.JsonReader;

import com.example.danco.homework2.provider.WeatherContract;

import java.io.IOException;
import java.util.Calendar;

/**
 * @author danco on 4/18/15.
 */
public class CurrentConditionsProcessor extends BaseProcessor {

    public static CurrentConditionsProcessor newInstance() {
        return new CurrentConditionsProcessor();
    }

    private CurrentConditionsProcessor() {
    }


    @Override
    protected ContentValues processObject(JsonReader jsonReader)
            throws IOException {

        ContentValues values = new ContentValues();
        values.put(WeatherContract.WeatherColumns.ADDED,
                Calendar.getInstance().getTime().getTime());

        jsonReader.beginObject();

        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();
            switch (name) {
                case WeatherContract.CurrentConditionsJson.MAIN:
                    values.putAll(processSubobject(jsonReader));
                    break;
                case WeatherContract.CurrentConditionsJson.WEATHER:
                    values.putAll(processWeather(jsonReader));
                    break;
                case WeatherContract.CurrentConditionsJson.NAME:
                    values.put(WeatherContract.CurrentConditionsColumns.CITY,
                            jsonReader.nextString());
                    break;
                case WeatherContract.CurrentConditionsJson.TIMESTAMP:
                    values.put(WeatherContract.CurrentConditionsColumns.TIMESTAMP,
                            jsonReader.nextLong() * 1000);
                    break;
                case WeatherContract.CurrentConditionsJson.WIND:
                    values.putAll(processSubobject(jsonReader));
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }

        jsonReader.endObject();

        return values;
    }


    /**
     * extract the temp values from the main object
     * @param jsonReader
     * @return
     */
    private static ContentValues processSubobject(JsonReader jsonReader) throws IOException {
        ContentValues values = new ContentValues();

        jsonReader.beginObject();

        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();
            switch (name) {
                case WeatherContract.CurrentConditionsJson.TEMP:
                    values.put(WeatherContract.CurrentConditionsColumns.TEMP,
                            jsonReader.nextDouble());
                    break;
                case WeatherContract.CurrentConditionsJson.TEMP_MIN:
                    values.put(WeatherContract.CurrentConditionsColumns.TEMP_MIN,
                            jsonReader.nextDouble());
                    break;
                case WeatherContract.CurrentConditionsJson.TEMP_MAX:
                    values.put(WeatherContract.CurrentConditionsColumns.TEMP_MAX,
                            jsonReader.nextDouble());
                    break;
                case WeatherContract.CurrentConditionsJson.HUMIDITY:
                    values.put(WeatherContract.CurrentConditionsColumns.HUMIDITY,
                            jsonReader.nextDouble());
                    break;
                case WeatherContract.CurrentConditionsJson.SPEED:
                    values.put(WeatherContract.CurrentConditionsColumns.WIND,
                            jsonReader.nextDouble());
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }

        jsonReader.endObject();

        return values;
    }
}
