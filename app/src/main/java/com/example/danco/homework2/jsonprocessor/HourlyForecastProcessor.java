package com.example.danco.homework2.jsonprocessor;

import android.content.ContentValues;
import android.util.JsonReader;
import android.util.Log;

import com.example.danco.homework2.provider.WeatherContract;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author danco on 4/18/15.
 */
public class HourlyForecastProcessor extends BaseProcessor {
    private static final String TAG = HourlyForecastProcessor.class.getSimpleName() + "TAG";

    public static HourlyForecastProcessor newInstance() {
        return new HourlyForecastProcessor();
    }

    private HourlyForecastProcessor() {
    }


    @Override
    public ContentValues[] buildContentValues(InputStream stream) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8"));
        String cityName = "";

        ContentValues[] values = null;
        // Note: AS will indicate that you can use try with resources because Java 7 is
        // target. However, try with resources is not compatible prior to SDK 19.
        try {
            reader.beginObject();

            while (reader.hasNext()) {
                String name = reader.nextName();
                switch (name) {
                    case WeatherContract.HourlyForecastJson.CITY:
                        cityName = processCity(reader);
                        break;
                    case WeatherContract.HourlyForecastJson.DAY_LIST:
                        values = processArray(reader);
                        for (ContentValues cv : values) {
                            cv.put(WeatherContract.DailyForecastColumns.CITY, cityName);
                            cv.put(WeatherContract.WeatherColumns.ADDED,
                                    Calendar.getInstance().getTime().getTime());
                        }
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }

            return values;
        }
        finally {
            reader.close();
        }
    }


    @Override
    protected ContentValues processObject(JsonReader reader) throws IOException {
        ContentValues values = new ContentValues();

        reader.beginObject();

        while (reader.hasNext()) {
            String name = reader.nextName();
            switch (name) {
                case WeatherContract.HourlyForecastJson.TIMESTAMP:
                    values.put(WeatherContract.HourlyForecastJson.TIMESTAMP,
                            reader.nextLong() * 1000);
                    break;
                case WeatherContract.HourlyForecastJson.MAIN:
                    values.putAll(processMain(reader));
                    break;
                case WeatherContract.DailyForecastJson.WEATHER:
                    values.putAll(processWeather(reader));
                    break;
                case WeatherContract.HourlyForecastJson.HOURLY_TIMESTAMP:
                    String dtTxt = reader.nextString();
                    values.put(WeatherContract.HourlyForecastJson.HOURLY_TIMESTAMP,
                            dtTxt);
                    DateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date forecastDate = null;
                    try {
                        forecastDate = dateParser.parse(dtTxt);
                    } catch (ParseException e) {
                        Log.e(TAG, "Unable to parse date from " + dtTxt);
                    }
                    DateFormat formatter = new SimpleDateFormat("EEE");
                    values.put(WeatherContract.HourlyForecastJson.DAY,
                            formatter.format(forecastDate));
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }

        reader.endObject();

        return values;
    }


    /**
     * extract the temp values from the temp object
     * @param reader
     * @return
     */
    private static ContentValues processMain(JsonReader reader) throws IOException {
        ContentValues values = new ContentValues();

        reader.beginObject();

        while (reader.hasNext()) {
            String name = reader.nextName();
            switch (name) {
                case WeatherContract.HourlyForecastJson.TEMP:
                    values.put(WeatherContract.HourlyForecastJson.TEMP,
                            reader.nextDouble());
                    break;
                case WeatherContract.HourlyForecastJson.TEMP_MIN:
                    values.put(WeatherContract.HourlyForecastJson.TEMP_MIN,
                            reader.nextDouble());
                    break;
                case WeatherContract.HourlyForecastJson.TEMP_MAX:
                    values.put(WeatherContract.HourlyForecastJson.TEMP_MAX,
                            reader.nextDouble());
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }

        reader.endObject();

        return values;
    }
}
