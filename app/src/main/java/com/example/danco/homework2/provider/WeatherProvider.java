package com.example.danco.homework2.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;

/**
 * @author danco on 4/14/15.
 *
 * http://api.openweathermap.org/data/2.5/weather?id=5809844&units=imperial&APPID=
 */
public class WeatherProvider extends ContentProvider {
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private static final int CURRENT_CONDITIONS = 1;
    private static final int CURRENT_CONDITIONS_ID = 2;
    private static final int DAILY_FORECAST = 3;
    private static final int DAILY_FORECAST_ID = 4;
    private static final int HOURLY_FORECAST = 5;
    private static final int HOURLY_FORECAST_ID = 6;

    static {
        uriMatcher.addURI(WeatherContract.AUTHORITY,
                WeatherContract.CURRENT_CONDITIONS_TABLE, CURRENT_CONDITIONS);
        uriMatcher.addURI(WeatherContract.AUTHORITY,
                WeatherContract.CURRENT_CONDITIONS_TABLE + "/#", CURRENT_CONDITIONS_ID);

        uriMatcher.addURI(WeatherContract.AUTHORITY,
                WeatherContract.DAILY_FORECAST_TABLE, DAILY_FORECAST);
        uriMatcher.addURI(WeatherContract.AUTHORITY,
                WeatherContract.DAILY_FORECAST_TABLE + "/#", DAILY_FORECAST_ID);

        uriMatcher.addURI(WeatherContract.AUTHORITY,
                WeatherContract.HOURLY_FORECAST_TABLE, HOURLY_FORECAST);
        uriMatcher.addURI(WeatherContract.AUTHORITY,
                WeatherContract.HOURLY_FORECAST_TABLE + "/#", HOURLY_FORECAST_ID);
    }

    private static final String WHERE_MATCHES_ID = BaseColumns._ID + " = ?";

    private DBHelper dbHelper;


    @Override
    public boolean onCreate() {
        dbHelper = DBHelper.getInstance(getContext());
        return true;
    }


    @Override
    public String getType(Uri uri) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case CURRENT_CONDITIONS:
                return WeatherContract.CURRENT_CONDITIONS_TYPE;
            case CURRENT_CONDITIONS_ID:
                return WeatherContract.CURRENT_CONDITIONS_ITEM_TYPE;
            case DAILY_FORECAST:
                return WeatherContract.DAILY_FORECAST_TYPE;
            case DAILY_FORECAST_ID:
                return WeatherContract.DAILY_FORECAST_ITEM_TYPE;
            case HOURLY_FORECAST:
                return WeatherContract.HOURLY_FORECAST_TYPE;
            case HOURLY_FORECAST_ID:
                return WeatherContract.HOURLY_FORECAST_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }


    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        int match = uriMatcher.match(uri);
        String table = null;
        switch (match) {
            case CURRENT_CONDITIONS:
                table = WeatherContract.CURRENT_CONDITIONS_TABLE;
                break;
            case DAILY_FORECAST:
                table = WeatherContract.DAILY_FORECAST_TABLE;
                break;
            case HOURLY_FORECAST:
                table = WeatherContract.HOURLY_FORECAST_TABLE;
                break;
            case CURRENT_CONDITIONS_ID:
            case DAILY_FORECAST_ID:
            case HOURLY_FORECAST_ID:
                throw new UnsupportedOperationException("Unable to insert by id. uri: " + uri);
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        long id = -1;
        db.beginTransactionNonExclusive();
        try {
            id = db.insertWithOnConflict(table, null,
                    contentValues, SQLiteDatabase.CONFLICT_REPLACE);
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
        }

        // notify change essentially indicates to any users with active cursors
        // that they need to "reload" the data
        notifyChange(uri);
        return ContentUris.withAppendedId(uri, id);
    }


    @Override
    public int bulkInsert(Uri uri, @NonNull ContentValues[] valuesArray) {
        int match = uriMatcher.match(uri);
        String table = null;
        switch (match) {
            case CURRENT_CONDITIONS:
                table = WeatherContract.CURRENT_CONDITIONS_TABLE;
                break;
            case DAILY_FORECAST:
                table = WeatherContract.DAILY_FORECAST_TABLE;
                break;
            case HOURLY_FORECAST:
                table = WeatherContract.HOURLY_FORECAST_TABLE;
                break;
            case CURRENT_CONDITIONS_ID:
            case DAILY_FORECAST_ID:
            case HOURLY_FORECAST_ID:
                throw new UnsupportedOperationException("Unable to insert by id. uri: " + uri);
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        int count = 0;
        db.beginTransactionNonExclusive();
        try {
            for (ContentValues values : valuesArray) {
                long id = db.insertWithOnConflict(table, null,
                        values, SQLiteDatabase.CONFLICT_REPLACE);
                if (id != WeatherContract.NO_ID) {
                    ++count;
                }
            }
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
        }

        // notify change essentially indicates to any users with active cursors
        // that they need to "reload" the data
        if (count > 0) {
            notifyChange(uri);
        }
        return count;
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        int match = uriMatcher.match(uri);
        String useSelection = selection;
        String[] useSelectionArgs = selectionArgs;
        String table = null;

        switch (match) {
            case CURRENT_CONDITIONS_ID:
                table = WeatherContract.CURRENT_CONDITIONS_TABLE;
                useSelection = WHERE_MATCHES_ID;
                useSelectionArgs = new String[]{uri.getLastPathSegment()};
                break;
            case DAILY_FORECAST_ID:
                table = WeatherContract.DAILY_FORECAST_TABLE;
                useSelection = WHERE_MATCHES_ID;
                useSelectionArgs = new String[]{uri.getLastPathSegment()};
                break;
            case HOURLY_FORECAST_ID:
                table = WeatherContract.HOURLY_FORECAST_TABLE;
                useSelection = WHERE_MATCHES_ID;
                useSelectionArgs = new String[]{uri.getLastPathSegment()};
                break;
            case CURRENT_CONDITIONS:
                table = WeatherContract.CURRENT_CONDITIONS_TABLE;
                break;
            case DAILY_FORECAST:
                table = WeatherContract.DAILY_FORECAST_TABLE;
                break;
            case HOURLY_FORECAST:
                table = WeatherContract.HOURLY_FORECAST_TABLE;
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor result = db.query(table, projection,
                useSelection, useSelectionArgs, null, null, sortOrder);
        // Register the cursor with the requested CURRENT_CONDITIONS_URI so the caller will receive
        // future database change notifications. Useful for "loaders" which take advantage
        // of this concept.
        result.setNotificationUri(getContext().getContentResolver(), uri);
        return result;
    }


    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        String useSelection = selection;
        String[] useSelectionArgs = selectionArgs;

        String table = null;

        switch (match) {
            case CURRENT_CONDITIONS_ID:
                table = WeatherContract.CURRENT_CONDITIONS_TABLE;
                useSelection = WHERE_MATCHES_ID;
                useSelectionArgs = new String[]{uri.getLastPathSegment()};
                break;
            case DAILY_FORECAST_ID:
                table = WeatherContract.DAILY_FORECAST_TABLE;
                useSelection = WHERE_MATCHES_ID;
                useSelectionArgs = new String[]{uri.getLastPathSegment()};
                break;
            case HOURLY_FORECAST_ID:
                table = WeatherContract.HOURLY_FORECAST_TABLE;
                useSelection = WHERE_MATCHES_ID;
                useSelectionArgs = new String[]{uri.getLastPathSegment()};
                break;
            case CURRENT_CONDITIONS:
                table = WeatherContract.CURRENT_CONDITIONS_TABLE;
                break;
            case DAILY_FORECAST:
                table = WeatherContract.DAILY_FORECAST_TABLE;
                break;
            case HOURLY_FORECAST:
                table = WeatherContract.HOURLY_FORECAST_TABLE;
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int rows = 0;
        db.beginTransactionNonExclusive();
        try {
            rows = db.update(table, values, useSelection, useSelectionArgs);
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
        }

        // notify change essentially indicates to any users with active cursors
        // that they need to "reload" the data
        if (rows > 0) {
            notifyChange(uri);
        }
        return rows;
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        String useSelection = selection;
        String[] useSelectionArgs = selectionArgs;

        String table = null;

        switch (match) {
            case CURRENT_CONDITIONS_ID:
                table = WeatherContract.CURRENT_CONDITIONS_TABLE;
                useSelection = WHERE_MATCHES_ID;
                useSelectionArgs = new String[]{uri.getLastPathSegment()};
                break;
            case DAILY_FORECAST_ID:
                table = WeatherContract.DAILY_FORECAST_TABLE;
                useSelection = WHERE_MATCHES_ID;
                useSelectionArgs = new String[]{uri.getLastPathSegment()};
                break;
            case HOURLY_FORECAST_ID:
                table = WeatherContract.HOURLY_FORECAST_TABLE;
                useSelection = WHERE_MATCHES_ID;
                useSelectionArgs = new String[]{uri.getLastPathSegment()};
                break;
            case CURRENT_CONDITIONS:
                table = WeatherContract.CURRENT_CONDITIONS_TABLE;
                break;
            case DAILY_FORECAST:
                table = WeatherContract.DAILY_FORECAST_TABLE;
                break;
            case HOURLY_FORECAST:
                table = WeatherContract.HOURLY_FORECAST_TABLE;
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Non Exclusive required for write ahead logging
        int rows = 0;
        db.beginTransactionNonExclusive();
        try {
            rows = db.delete(table, useSelection, useSelectionArgs);
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
        }

        // notify change essentially indicates to any users with active cursors
        // that they need to "reload" the data
        if (rows > 0) {
            notifyChange(uri);
        }
        return rows;
    }


    /**
     * Helper method to notify listeners of the changes to the database. Useful with loaders
     *
     * @param uri the CURRENT_CONDITIONS_URI for the content that changed.
     */
    private void notifyChange(Uri uri) {
        getContext().getContentResolver().notifyChange(uri, null, false);
    }
}
