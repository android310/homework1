package com.example.danco.homework2.fragment;


import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.danco.homework2.R;
import com.example.danco.homework2.adapter.HourlyForecastAdapter;
import com.example.danco.homework2.loader.HourlyForecastLoaderCallbacks;
import com.example.danco.homework2.provider.WeatherContract;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HourlyForecastFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HourlyForecastFragment extends BaseWeatherFragment
        implements HourlyForecastLoaderCallbacks.OnWeatherLoaderListener,
        AdapterView.OnItemClickListener {
    private static final String TAG = HourlyForecastFragment.class.getSimpleName() + ".TAG";

    private static final String ARG_DAY_OF_WEEK = "dayOfWeek";

    private static String dayOfWeek;

    private static final String WHERE_DAY_EQUAL =
            WeatherContract.HourlyForecastColumns.DAY_OF_WEEK + " = ?";

    private OnHourlyForecastListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BlankFragment.
     */
    public static HourlyForecastFragment newInstance() {
        Log.i(TAG, "creating HourlyForecastFragment");
        HourlyForecastFragment fragment = new HourlyForecastFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    public static HourlyForecastFragment newInstance(String dayOfWeek) {
        Log.i(TAG, "creating HourlyForecastFragment");
        HourlyForecastFragment fragment = new HourlyForecastFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DAY_OF_WEEK, dayOfWeek);
        fragment.setArguments(args);
        return fragment;
    }


    public HourlyForecastFragment() {
        // Required empty public constructor
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnHourlyForecastListener {
        // TODO: Update argument type and name
        public void onHourlyFragmentInteraction(int position);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        animationDuration = activity.getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        try {
            mListener = (OnHourlyForecastListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHourlyForecastListener");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hourly_forecast, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null && getArguments().getString(ARG_DAY_OF_WEEK) != null) {
            dayOfWeek = getArguments().getString(ARG_DAY_OF_WEEK);
        } else {
            dayOfWeek = null;
        }

        ViewHolder holder = new ViewHolder(view);
        view.setTag(holder);

        holder.list.setAdapter(new HourlyForecastAdapter(view.getContext()));
        holder.listContainer.setVisibility(View.GONE);
        holder.progress.setVisibility(View.VISIBLE);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        HourlyForecastLoaderCallbacks.initLoader(getActivity(), getLoaderManager(), this,
                HourlyForecastAdapter.PROJECTION);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onLoadComplete(@Nullable Cursor cursor) {
        Log.i(TAG, "load complete");
        ViewHolder holder = getViewHolder();
        if (holder == null) return;

        HourlyForecastAdapter adapter = (HourlyForecastAdapter) holder.list.getAdapter();
        if (dayOfWeek != null) {
            cursor = getActivity().getContentResolver().query(
                    WeatherContract.HOURLY_FORECAST_URI,
                    HourlyForecastAdapter.PROJECTION, WHERE_DAY_EQUAL,
                    new String[]{dayOfWeek}, null);
        }
        adapter.swapCursor(cursor);
        if (holder.listContainer.getVisibility() != View.VISIBLE) {
            crossFadeViews(holder.listContainer, holder.progress);
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.i(TAG, "clicked item " + position);
        mListener.onHourlyFragmentInteraction(position);
    }


    private @Nullable ViewHolder getViewHolder() {
        View view = getView();
        return view != null ? (ViewHolder) view.getTag() : null;
    }

    /* package */ class ViewHolder {
        final View listContainer;
        final ListView list;
        final View progress;

        ViewHolder(View view) {
            listContainer = view.findViewById(R.id.list_container);
            list = (ListView) listContainer.findViewById(R.id.list);
            list.setEmptyView(listContainer.findViewById(R.id.empty));
            list.setOnItemClickListener(HourlyForecastFragment.this);
            progress = view.findViewById(R.id.progress);
        }
    }
}
