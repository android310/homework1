package com.example.danco.homework2.provider;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * @author costd037 on 4/22/15.
 */
public class CityContract {
    public interface Columns extends BaseColumns {
        // _ID provided by base columns
        public String NAME = "name";
    }

    /* package */ static final String TABLE = "city";

    // Provider URIs
    // The authority needs to be globally unique. It should match what is in the manifest.
    // Easiest way to make unique is use the fully qualified class name of the provider.
    /* package */ static final String AUTHORITY = WeatherProvider.class.getName();

    // The base URI for all other URIs. Starts with "content" to indicate it will come
    // from a content provider. Also references the Authority
    // Will have final form of: content://com.example.joeroger.samplestorage.provider.StateProvider
    /* package */ static final Uri BASE_URI = new Uri.Builder()
            .scheme(ContentResolver.SCHEME_CONTENT)
            .authority(AUTHORITY)
            .build();


    // Should have one URI per table (or path), supported by this contract. In most cases the
    // contract should only support one table in which case the BASE_URI can be the URI.
    //
    // However, you may have situations where you need more than one.
    // In that case build upon the base URI and add a "path" for each table you support.
    // Will have final form of:
    // content://com.example.joeroger.samplestorage.provider.StateProvider/state
    //
    // public static final Uri URI = BASE_URI.buildUpon()
    //        .appendPath(TABLE)
    //        .build();

    // In this provider's case there is only one table, so lets just use the base uri. It has
    // the advantage is the "table" name can not be guessed if exported.
    public static final Uri URI = BASE_URI;


    // These are the "types" of data that may be returned for various URIs. The first is when multiple
    // items are returned. For example fetch all states.  The second is used when a specific item is returned.
    public static final String CONTENT_TYPE =
            ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + AUTHORITY + "/" + TABLE;

    public static final String CONTENT_ITEM_TYPE =
            ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + AUTHORITY + "/" + TABLE;

    // Database statements
    /* package */ static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE + " ( " +
                    BaseColumns._ID + " INTEGER PRIMARY KEY, " +
                    Columns.NAME + " TEXT UNIQUE NOT NULL" +
                    " )";
}
