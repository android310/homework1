package com.example.danco.homework2.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.View;

/**
 * @author danco on 4/18/15.
 */
public class BaseWeatherFragment extends Fragment {
    protected long animationDuration;

    protected void crossFadeViews(final View fadeInView, final View fadeOutView) {
        fadeInView.setAlpha(0f);
        fadeInView.setVisibility(View.VISIBLE);

        ViewCompat.animate(fadeInView)
                .alpha(1f)
                .setDuration(animationDuration)
                .setListener(null)
                .withLayer();

        ViewCompat.animate(fadeOutView)
                .alpha(0f)
                .setDuration(animationDuration)
                .setListener(null)
                .withLayer()
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        fadeOutView.setVisibility(View.GONE);
                    }
                });
    }

}
