package com.example.danco.homework2.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.danco.homework2.R;
import com.example.danco.homework2.provider.WeatherContract;

/**
 * @author danco on 4/17/15.
 */
public class CurrentConditionsAdapter extends CursorAdapter {
    public static final String[] PROJECTION = {
            WeatherContract.CurrentConditionsColumns._ID,
            WeatherContract.CurrentConditionsColumns.ICON,
            WeatherContract.CurrentConditionsColumns.CITY,
            WeatherContract.CurrentConditionsColumns.DESCRIPTION,
            WeatherContract.CurrentConditionsColumns.TEMP,
            WeatherContract.CurrentConditionsColumns.HUMIDITY,
            WeatherContract.CurrentConditionsColumns.WIND
    };

    private static final int ICON_POS = 1;
    private static final int CITY_POS = 2;
    private static final int DESCRIPTION_POS = 3;
    private static final int TEMP_POS = 4;
    private static final int HUMIDITY_POS = 5;
    private static final int WIND_POS = 6;

    public CurrentConditionsAdapter(Context context) {
        super(context, null, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.current_conditions,
                parent, false);
        view.setTag(new ViewHolder(view));

        return view;
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
//        holder.icon.setImageURI(new Uri.Builder()
//                .scheme("http")
//                .authority("openweathermap.org")
//                .appendPath("img").appendPath("w")
//                .appendPath(cursor.getString(ICON_POS) + ".png")
//                .build());
        holder.description.setText(cursor.getString(DESCRIPTION_POS));
        holder.temp.setText(cursor.getString(TEMP_POS) + " degrees");
        holder.humidityLabel.setText("Humidity:");
        holder.humidity.setText(Long.toString(cursor.getLong(HUMIDITY_POS)));
        holder.windSpeedLabel.setText("Wind speed:");
        holder.windSpeed.setText(Double.toString(cursor.getDouble(WIND_POS)));
    }


    /* package */ class ViewHolder {
        final ImageView icon;
        final TextView description;
        final TextView temp;
        final TextView humidityLabel;
        final TextView humidity;
        final TextView windSpeedLabel;
        final TextView windSpeed;
        ViewHolder(View view) {
            icon = (ImageView) view.findViewById(R.id.weatherIcon);
            description = (TextView) view.findViewById(R.id.currentDescription);
            temp = (TextView) view.findViewById(R.id.currentTemp);
            humidityLabel = (TextView) view.findViewById(R.id.humidityLabel);
            humidity = (TextView) view.findViewById(R.id.humidity);
            windSpeedLabel = (TextView) view.findViewById(R.id.windSpeedLabel);
            windSpeed = (TextView) view.findViewById(R.id.windSpeed);
        }
    }
}
