package com.example.danco.homework2.jsonprocessor;

import android.content.ContentValues;
import android.util.JsonReader;

import com.example.danco.homework2.provider.WeatherContract;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;

/**
 * @author danco on 4/18/15.
 */
public class DailyForecastProcessor extends BaseProcessor {

    public static DailyForecastProcessor newInstance() {
        return new DailyForecastProcessor();
    }

    private DailyForecastProcessor() {
    }


    @Override
    public ContentValues[] buildContentValues(InputStream stream) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8"));
        String cityName = "";

        ContentValues[] values = null;
        // Note: AS will indicate that you can use try with resources because Java 7 is
        // target. However, try with resources is not compatible prior to SDK 19.
        try {
            reader.beginObject();

            while (reader.hasNext()) {
                String name = reader.nextName();
                switch (name) {
                    case WeatherContract.DailyForecastJson.CITY:
                        cityName = processCity(reader);
                        break;
                    case WeatherContract.DailyForecastJson.DAY_LIST:
                        values = processArray(reader);
                        for (ContentValues cv : values) {
                            cv.put(WeatherContract.DailyForecastColumns.CITY, cityName);
                            cv.put(WeatherContract.WeatherColumns.ADDED,
                                    Calendar.getInstance().getTime().getTime());
                        }
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }

            return values;
        }
        finally {
            reader.close();
        }
    }


    @Override
    protected ContentValues processObject(JsonReader reader) throws IOException {
        ContentValues values = new ContentValues();

        reader.beginObject();

        while (reader.hasNext()) {
            String name = reader.nextName();
            switch (name) {
                case WeatherContract.DailyForecastJson.TIMESTAMP:
                    values.put(WeatherContract.DailyForecastJson.TIMESTAMP,
                            reader.nextLong() * 1000);
                    break;
                case WeatherContract.DailyForecastJson.TEMP:
                    values.putAll(processSubobject(reader));
                    break;
                case WeatherContract.DailyForecastJson.WEATHER:
                    values.putAll(processWeather(reader));
                    break;
                case WeatherContract.DailyForecastJson.HUMIDITY:
                    values.put(WeatherContract.DailyForecastJson.HUMIDITY,
                            reader.nextDouble());
                    break;
                case WeatherContract.DailyForecastJson.SPEED:
                    values.put(WeatherContract.DailyForecastJson.WIND,
                            reader.nextDouble());
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }

        reader.endObject();

        return values;
    }


    /**
     * extract the temp values from the temp object
     * @param reader
     * @return
     */
    private static ContentValues processSubobject(JsonReader reader) throws IOException {
        ContentValues values = new ContentValues();

        reader.beginObject();

        while (reader.hasNext()) {
            String name = reader.nextName();
            switch (name) {
                case WeatherContract.DailyForecastJson.TEMP_MIN:
                    values.put(WeatherContract.DailyForecastColumns.MIN,
                            reader.nextDouble());
                    break;
                case WeatherContract.DailyForecastJson.TEMP_MAX:
                    values.put(WeatherContract.DailyForecastColumns.MAX,
                            reader.nextDouble());
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }

        reader.endObject();

        return values;
    }
}
