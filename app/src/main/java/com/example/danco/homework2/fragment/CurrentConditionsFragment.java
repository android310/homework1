package com.example.danco.homework2.fragment;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.danco.homework2.adapter.CurrentConditionsAdapter;
import com.example.danco.homework2.R;
import com.example.danco.homework2.loader.CurrentConditionsLoaderCallbacks;

/**
 * @author danco on 4/12/15.
 */
public class CurrentConditionsFragment extends BaseWeatherFragment
        implements CurrentConditionsLoaderCallbacks.OnWeatherLoaderListener {
    private static final String TAG = CurrentConditionsFragment.class.getSimpleName() + ".TAG";

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment DailyForecast.
     */
    public static CurrentConditionsFragment newInstance() {
        Log.i(TAG, "creating CurrentConditionsFragment");
        CurrentConditionsFragment fragment = new CurrentConditionsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    public CurrentConditionsFragment() {
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        animationDuration = activity.getResources().getInteger(
                android.R.integer.config_shortAnimTime);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_current_conditions, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ViewHolder holder = new ViewHolder(view);
        view.setTag(holder);

        holder.list.setAdapter(new CurrentConditionsAdapter(view.getContext()));
        holder.listContainer.setVisibility(View.GONE);
        holder.progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CurrentConditionsLoaderCallbacks.initLoader(getActivity(), getLoaderManager(), this,
                CurrentConditionsAdapter.PROJECTION);
    }

    @Override
    public void onLoadComplete(@Nullable Cursor cursor) {
        ViewHolder holder = getViewHolder();
        if (holder == null) return;

        CurrentConditionsAdapter adapter = (CurrentConditionsAdapter) holder.list.getAdapter();
        adapter.swapCursor(cursor);
        if (holder.listContainer.getVisibility() != View.VISIBLE) {
            crossFadeViews(holder.listContainer, holder.progress);
        }
    }

    private @Nullable ViewHolder getViewHolder() {
        View view = getView();
        return view != null ? (ViewHolder) view.getTag() : null;
    }

    /* package */ class ViewHolder {
        final View listContainer;
        final ListView list;
        final View progress;

        ViewHolder(View view) {
            listContainer = view.findViewById(R.id.list_container);
            list = (ListView) listContainer.findViewById(R.id.list);
            list.setEmptyView(listContainer.findViewById(R.id.empty));
            progress = view.findViewById(R.id.progress);
        }
    }
}
