package com.example.danco.homework2.loader;

/**
 * @author danco on 4/6/15.
 */
public interface LoaderIds {
    public static final int CURRENT_CONDITIONS_LOADER = 100;
    public static final int DAILY_FORECAST_LOADER = 200;
    public static final int HOURLY_FORECAST_LOADER = 300;
}
