package com.example.danco.homework2.loader;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;

import com.example.danco.homework2.provider.WeatherContract;

import java.lang.ref.WeakReference;

/**
 * @author danco on 4/18/15.
 */
public class DailyForecastLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = CurrentConditionsLoaderCallbacks.class.getSimpleName() + ".TAG";
    private static final String ARG_URI = "Uri";
    private static final String ARG_PROJECTION = "projection";

    public interface OnWeatherLoaderListener {
        void onLoadComplete(@Nullable Cursor cursor);
    }

    private final Context applicationContext;
    private final WeakReference<OnWeatherLoaderListener> listenerRef;


    public static void initLoader(Context context, LoaderManager loaderManager,
                                  OnWeatherLoaderListener listener, String[] projection) {
        initLoader(context, loaderManager, listener, WeatherContract.NO_ID,
                projection);
    }


    public static void initLoader(Context context, LoaderManager loaderManager,
                                  OnWeatherLoaderListener listener, long id,
                                  String[] projection) {
        Bundle args = new Bundle();
        args.putParcelable(ARG_URI, id == WeatherContract.NO_ID ?
                WeatherContract.DAILY_FORECAST_URI :
                ContentUris.withAppendedId(WeatherContract.DAILY_FORECAST_URI, id));
        args.putStringArray(ARG_PROJECTION, projection);
        loaderManager.initLoader(LoaderIds.DAILY_FORECAST_LOADER, args,
                new DailyForecastLoaderCallbacks(context, listener));
    }


    protected DailyForecastLoaderCallbacks(Context applicationContext,
                                             OnWeatherLoaderListener listenerRef) {
        this.applicationContext = applicationContext;
        this.listenerRef = new WeakReference<>(listenerRef);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Uri uri = bundle.getParcelable(ARG_URI);
        return new CursorLoader(applicationContext,
                uri,
                bundle.getStringArray(ARG_PROJECTION),
                null,
                null,
                WeatherContract.CurrentConditionsColumns.CITY + " desc");
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        Log.i(TAG, "loader finished");
        OnWeatherLoaderListener listener = listenerRef.get();
        if (listener != null) {
            listener.onLoadComplete(cursor);
        }
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        onLoadFinished(loader, null);
    }
}
