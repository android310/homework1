package com.example.danco.homework2.activity;

import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.danco.homework2.R;
import com.example.danco.homework2.fragment.CurrentConditionsFragment;
import com.example.danco.homework2.fragment.DailyForecastFragment;
import com.example.danco.homework2.fragment.HourlyForecastFragment;
import com.example.danco.homework2.service.NetworkIntentService;


public class MainActivity extends ActionBarActivity implements
        AdapterView.OnItemSelectedListener,
        DailyForecastFragment.OnDailyForecastListener,
        HourlyForecastFragment.OnHourlyForecastListener {

    private static final String LOG_TAG = MainActivity.class.getSimpleName() + ".LOG_TAG";
    private static final String EXTRA_POSITION = MainActivity.class.getSimpleName() + ".position";

    // maybe implement another callback for the CurrentConditions fragment to pass back the
    // City name and set the toolbar with that data then?

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        if (savedInstanceState == null) {
//            getSupportFragmentManager().beginTransaction()
//                                       .add(R.id.container, new PlaceholderFragment())
//                                       .commit();
//        }

        Toolbar tb = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar(tb);
        getSupportActionBar().setElevation(getResources().getDimensionPixelSize(
                R.dimen.actionbar_elevation));

        NetworkIntentService.startService(this);

        // Disable the default title
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        int position = getIntent().getIntExtra(EXTRA_POSITION, 0);

        // Setup the navigation spinner
        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        // Use action bar's themed context for spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                // Using themed context to ensure text is white...
                getSupportActionBar().getThemedContext(),
                R.layout.item_spinner_title,
                android.R.id.text1,
                getResources().getStringArray(R.array.weather_fragments));

        adapter.setDropDownViewResource(R.layout.spinner_dropdown);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        // If somehow the position is beyond the array size, reset to 0.
        if (position >= adapter.getCount()) {
            position = 0;
        }
        spinner.setSelection(position);
        updateFragment(position);

        // If toolbar elevated, then the spinner also tries to be elevated.
        // This looks ugly on Android 5.0+. Set elevation to 0 to prevent
        ViewCompat.setElevation(spinner, 0);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.i(LOG_TAG, "onItemSelected, position = " + position);
        updateFragment(position);
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // this method intentionally left blank
    }


    private void updateFragment(int position) {
        Fragment fragment = null;

        String tag = "FRAG";

        Log.i(LOG_TAG, "position = " + position);
        switch (position) {
            case 0:
                fragment = CurrentConditionsFragment.newInstance();
                tag += "CurrentConditions";
                break;
            case 1:
                fragment = DailyForecastFragment.newInstance();
                tag += "DailyForecast";
                break;
            case 2:
                fragment = HourlyForecastFragment.newInstance();
                tag += "HourlyForecast";
                break;
            default:
                Toast.makeText(this, "No fragment defined for position " +
                        position, Toast.LENGTH_SHORT).show();
        }

        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, fragment, tag)
                    .commit();
        }
    }


    @Override
    public void onDailyForecastFragmentInteraction(String dayOfWeek) {
        //placeholder for future interaction handling
        Log.i(LOG_TAG, "HourlyForecast requested for " + dayOfWeek);
        Fragment fragment = HourlyForecastFragment.newInstance(dayOfWeek);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, fragment, dayOfWeek + "HourlyFrag")
                .commit();
    }


    @Override
    public void onHourlyFragmentInteraction(int position) {
        //placeholder for future interaction handling
        Log.i(LOG_TAG, "HourlyForecast listener event");
    }
}
