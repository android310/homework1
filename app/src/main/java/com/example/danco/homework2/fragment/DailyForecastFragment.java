package com.example.danco.homework2.fragment;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.danco.homework2.R;
import com.example.danco.homework2.adapter.DailyForecastAdapter;
import com.example.danco.homework2.loader.DailyForecastLoaderCallbacks;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnDailyForecastListener} interface
 * to handle interaction events.
 * Use the {@link DailyForecastFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DailyForecastFragment extends BaseWeatherFragment
        implements DailyForecastLoaderCallbacks.OnWeatherLoaderListener,
        AdapterView.OnItemClickListener {
    private static final String TAG = DailyForecastFragment.class.getSimpleName() + ".TAG";
    private OnDailyForecastListener mListener;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment DailyForecast.
     */
    public static DailyForecastFragment newInstance() {
        Log.i(TAG, "creating DailyForecastFragment");
        DailyForecastFragment fragment = new DailyForecastFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    public DailyForecastFragment() {
        // Required empty public constructor
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnDailyForecastListener {
        // TODO: Update argument type and name
        public void onDailyForecastFragmentInteraction(String dayOfWeek);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        animationDuration = activity.getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        try {
            mListener = (OnDailyForecastListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnDailyForecastListener");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_daily_forecast, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ViewHolder holder = new ViewHolder(view);
        view.setTag(holder);

        holder.list.setAdapter(new DailyForecastAdapter(view.getContext()));
        holder.listContainer.setVisibility(View.GONE);
        holder.progress.setVisibility(View.VISIBLE);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DailyForecastLoaderCallbacks.initLoader(getActivity(), getLoaderManager(), this,
                DailyForecastAdapter.PROJECTION);
    }


    @Override
    public void onLoadComplete(@Nullable Cursor cursor) {
        Log.i(TAG, "load complete");
        ViewHolder holder = getViewHolder();
        if (holder == null) return;

        DailyForecastAdapter adapter = (DailyForecastAdapter) holder.list.getAdapter();
        adapter.swapCursor(cursor);
        if (holder.listContainer.getVisibility() != View.VISIBLE) {
            crossFadeViews(holder.listContainer, holder.progress);
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.i(TAG, "clicked item " + position);
        TextView dayOfWeek = (TextView) view.findViewById(R.id.weekDay);
        mListener.onDailyForecastFragmentInteraction(dayOfWeek.getText().toString());
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private @Nullable ViewHolder getViewHolder() {
        View view = getView();
        return view != null ? (ViewHolder) view.getTag() : null;
    }

    /* package */ class ViewHolder {
        final View listContainer;
        final ListView list;
        final View progress;

        ViewHolder(View view) {
            listContainer = view.findViewById(R.id.list_container);
            list = (ListView) listContainer.findViewById(R.id.list);
            list.setEmptyView(listContainer.findViewById(R.id.empty));
            list.setOnItemClickListener(DailyForecastFragment.this);
            progress = view.findViewById(R.id.progress);
        }
    }

}
