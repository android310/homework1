package com.example.danco.homework2.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.danco.homework2.R;
import com.example.danco.homework2.provider.WeatherContract;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author danco on 4/18/15.
 */
public class DailyForecastAdapter extends CursorAdapter {
    public static final String[] PROJECTION = {
            WeatherContract.DailyForecastColumns._ID,
            WeatherContract.DailyForecastColumns.ICON,
            WeatherContract.DailyForecastColumns.CITY,
            WeatherContract.DailyForecastColumns.DESCRIPTION,
            WeatherContract.DailyForecastColumns.MIN,
            WeatherContract.DailyForecastColumns.MAX,
            WeatherContract.DailyForecastColumns.TIMESTAMP
    };

    private static final int ICON_POS = 1;
    private static final int CITY_POS = 2;
    private static final int DESCRIPTION_POS = 3;
    private static final int TEMP_MIN_POS = 4;
    private static final int TEMP_MAX_POS = 5;
    private static final int TIMESTAMP_POS = 6;

    public DailyForecastAdapter(Context context) {
        super(context, null, 0);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.daily_forecast,
                parent, false);
        view.setTag(new ViewHolder(view));

        return view;
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.description.setText(cursor.getString(DESCRIPTION_POS));
        holder.min.setText(cursor.getString(TEMP_MIN_POS));
        holder.max.setText(cursor.getString(TEMP_MAX_POS));
        holder.minLabel.setText("Min temp: ");
        holder.maxLabel.setText("Max temp: ");

        Date forecastDate = new Date(cursor.getLong(TIMESTAMP_POS));
        DateFormat formatter = new SimpleDateFormat("EEE", Locale.US);
        holder.dayOfWeek.setText(formatter.format(forecastDate));
    }


    /* package */ class ViewHolder {
        final ImageView icon;
        final TextView dayOfWeek;
        final TextView description;
        final TextView minLabel;
        final TextView maxLabel;
        final TextView min;
        final TextView max;
        ViewHolder(View view) {
            icon = (ImageView) view.findViewById(R.id.weatherIcon);
            dayOfWeek = (TextView) view.findViewById(R.id.weekDay);
            description = (TextView) view.findViewById(R.id.forecastDescription);
            minLabel = (TextView) view.findViewById(R.id.minTempLabel);
            maxLabel = (TextView) view.findViewById(R.id.maxTempLabel);
            min = (TextView) view.findViewById(R.id.minTemp);
            max = (TextView) view.findViewById(R.id.maxTemp);
        }
    }
}
